export function isObject(item) {
  return item && typeof item === 'object' && !Array.isArray(item);
}

export function objectToArray(target) {
  if (isObject(target)) {
    return Object.keys(target).reduce((acc, curr) => {
      acc.push(target[curr]);
      return acc;
    }, []);
  }
  return [];
}

export function arrayToObject(target, key) {
  return target.reduce((acc, curr) => {
    acc[curr[key]] = curr;
    return acc;
  }, {});
}

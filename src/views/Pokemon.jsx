import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/fontawesome-free-solid';

import { setPokemons } from '../actions/pokemons';

import PokePage from '../components/PokePage';

const getPokemon = idPokemon => gql`
  {
    pokemon(id: "${idPokemon}") {
      id
      name
      image
      types
      attacks {
        fast {
          name
          type
          damage
        }
        special {
          name
          type
          damage
        }
      }
    }
  }
`;

const Pokemon = () => {
  const { idPokemon } = useParams();
  const { data, loading, error } = useQuery(getPokemon(idPokemon));
  const { pokemons } = useSelector(state => state.pokemonsReducer);
  const history = useHistory();
  const dispatch = useDispatch();

  /* eslint-disable */
  useEffect(() => {
    if (!loading) {
      dispatch(
        setPokemons({ [data?.pokemon?.id]: data?.pokemon, ...pokemons })
      );
    }
  }, [loading]);
  /* eslint-enable */

  return (
    <div>
      <button
        onClick={() => history.push('/')}
        type="button"
        style={{ background: 'none', outline: 'none', border: 'none' }}
      >
        <FontAwesomeIcon
          icon={faArrowLeft}
          style={{
            fontSize: '35px',
            color: ' #495057',
            margin: '10px',
            cursor: 'pointer',
          }}
        />
      </button>
      {loading && <p>Carregando...</p>}
      {error && <p>Erro ao recuperar o pokemon {idPokemon}</p>}
      {!loading && !error && <PokePage idPokemon={idPokemon} />}
    </div>
  );
};
export default Pokemon;

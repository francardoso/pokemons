import React from 'react';
import { useLocation } from 'react-router-dom';

const Page404 = () => {
  const location = useLocation();
  return <p>Desculpe, não foi possivel achar {location.pathname}</p>;
};

export default Page404;

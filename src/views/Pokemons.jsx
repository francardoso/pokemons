import React, { useEffect } from 'react';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { useSelector, useDispatch } from 'react-redux';

import { arrayToObject } from '../utils/auxiliary-functions';
import { setPokemons } from '../actions/pokemons';

import PokeList from '../components/PokeList';

const GET_POKEMONS_INFO = gql`
  {
    pokemons(first: 10) {
      id
      name
      image
      types
      attacks {
        fast {
          name
          type
          damage
        }
        special {
          name
          type
          damage
        }
      }
    }
  }
`;

const Pokemons = () => {
  const { data, loading, error } = useQuery(GET_POKEMONS_INFO);
  const { pokemons } = useSelector(state => state.pokemonsReducer);
  const dispatch = useDispatch();

  /* eslint-disable */
  useEffect(() => {
    if (!loading) {
      const pokemonsObj = arrayToObject(data?.pokemons ?? [], 'id');
      dispatch(setPokemons({...pokemonsObj, ...pokemons }));
    }
  }, [loading]);
  /* eslint-enable */

  if (loading) return <p>Carregando...</p>;
  if (error) return <p>Erro ao recuperar os pokemons</p>;
  return <PokeList pokemons={pokemons} />;
};

export default Pokemons;

import { createStore, combineReducers } from 'redux';

import pokemonsReducer from './reducers/pokemons';

const reducer = combineReducers({ pokemonsReducer });

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;

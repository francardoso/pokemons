export const SET_POKEMONS = 'SET_POKEMONS';
export const UPDATE_POKEMON = 'UPDATE_POKEMON';

export const setPokemons = pokemons => ({
  type: SET_POKEMONS,
  payload: pokemons,
});

export const updatePokemon = pokemon => ({
  type: UPDATE_POKEMON,
  payload: pokemon,
});

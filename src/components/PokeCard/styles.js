import styled from 'styled-components/macro';

export default styled.div`
  flex: 1 0 40%;
  margin: 10px;
  border: 3px double black;
  padding: 10px;
  cursor: pointer;
  transition: transform 0.2s;
  &:hover {
    transform: scale(1.05);
  }

  #img-container {
    display: flex;
    justify-content: center;
    height: 200px;
    img {
      object-fit: contain;
      max-width: 200px;
      max-height: 200px;
    }
  }
  #name {
    margin: 5px;
    font-weight: 800;
    text-decoration: underline;
  }
  #types {
    margin: 5px;
    display: flex;
    flex-wrap: wrap;
    & > div {
      background-color: #1fc8db;
      background-image: linear-gradient(
        141deg,
        #9fb8ad 0%,
        #1fc8db 51%,
        #2cb5e8 75%
      );
      padding: 10px;
      margin-right: 5px;
      p {
        display: inline-block;
        color: #fff;
        text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
      }
    }
  }
`;

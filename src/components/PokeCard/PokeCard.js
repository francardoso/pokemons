import React from 'react';
import { useHistory } from 'react-router-dom';

import StyledDiv from './styles';

const PokeCard = ({ pokemon = {} }) => {
  const history = useHistory();
  return (
    <StyledDiv
      onClick={() => {
        history.push(`/${pokemon.id}`);
      }}
    >
      <div id="img-container">
        <img
          alt={pokemon?.name ?? 'pokemon'}
          src={pokemon?.image ?? 'https://i.imgflip.com/1n2sc4.jpg'}
        ></img>
      </div>
      <p id="name">{pokemon.name}</p>
      <div id="types">
        {pokemon?.types?.map((type, index) => (
          <div key={index}>
            <p>{type}</p>
          </div>
        ))}
      </div>
    </StyledDiv>
  );
};

export default PokeCard;

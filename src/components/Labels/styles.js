import styled from 'styled-components/macro';

export default styled.div`
  display: flex;
  .label-item {
    position: relative;
    display: inline-block;
    padding: 10px 20px 10px 10px;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0.25rem;
    color: #fff;
    background-color: #6c757d;
    margin: 0 5px;
    .icon-container {
      color: #fff;
      position: absolute;
      top: 0;
      right: 0;
      background: none;
      border: none;
      outline: none;
      cursor: pointer;
      transition: transform 0.2s;
      &:hover {
        transform: scale(1.05);
      }
    }
  }
`;

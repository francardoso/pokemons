import React from 'react';
import { render } from '@testing-library/react';
import Labels from './Labels';

describe('Labels', () => {
  it('should render two labels', () => {
    const { getAllByTestId } = render(<Labels items={['uma', 'outra']} />);
    const labels = getAllByTestId('label-item');
    expect(labels.length).toEqual(2);
  });
});

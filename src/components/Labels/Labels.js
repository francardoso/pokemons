import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faTimes, faPlus } from '@fortawesome/fontawesome-free-solid';

import Container from './styles';

const Labels = ({ items = [], onNewItem, onDeleteItem }) => {
  const [newItem, setNewItem] = useState(undefined);
  return (
    <Container>
      {items.map((item, index) => (
        <div key={index} className="label-item" data-testid="label-item">
          <button
            className="icon-container"
            type="button"
            onClick={() => onDeleteItem(item)}
          >
            <FontAwesomeIcon icon={faTimes} />
          </button>
          <p>{item}</p>
        </div>
      ))}
      {newItem !== undefined ? (
        <div>
          <input
            value={newItem || ''}
            onChange={e => setNewItem(e.target.value)}
            placeholder="insira um novo tipo"
          ></input>
          {newItem !== '' && (
            <button
              type="button"
              onClick={() => {
                onNewItem(newItem);
                setNewItem(undefined);
              }}
            >
              <FontAwesomeIcon icon={faCheck} />
            </button>
          )}
        </div>
      ) : (
        <button type="button" onClick={() => setNewItem('')}>
          <FontAwesomeIcon icon={faPlus} />
        </button>
      )}
    </Container>
  );
};

export default Labels;

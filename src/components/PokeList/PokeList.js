import React from 'react';

import PokeCard from '../PokeCard';

import StyledDiv from './styles';

const PokeList = ({ pokemons = {} }) => {
  if (pokemons.length === 0) return <p>Sem pokemons no momento</p>;
  return (
    <StyledDiv>
      {Object.values(pokemons).map(pokemon => (
        <PokeCard pokemon={pokemon} key={pokemon.id} />
      ))}
    </StyledDiv>
  );
};

export default PokeList;

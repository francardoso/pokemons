import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faCheck } from '@fortawesome/fontawesome-free-solid';
import { updatePokemon } from '../../actions/pokemons';

import Labels from '../Labels';

import StyledDiv from './styles';

const PokePage = ({ idPokemon }) => {
  const { pokemons } = useSelector(state => state.pokemonsReducer);
  const pokemon = pokemons[idPokemon];
  const [isEditingName, toggleEditionName] = useState(false);
  const dispatch = useDispatch();
  return (
    <StyledDiv>
      <div id="img-container">
        <img
          alt={pokemon?.name ?? 'pokemon'}
          src={pokemon?.image ?? 'https://i.imgflip.com/1n2sc4.jpg'}
        ></img>
      </div>
      <div id="name">
        <h5>Nome</h5>
        {isEditingName ? (
          <input
            value={pokemon?.name ?? ''}
            onChange={e => {
              dispatch(updatePokemon({ ...pokemon, name: e.target.value }));
            }}
          ></input>
        ) : (
          <p>{pokemon?.name}</p>
        )}
        <button
          className="icon-container"
          type="button"
          onClick={() => toggleEditionName(!isEditingName)}
        >
          {isEditingName ? (
            <FontAwesomeIcon icon={faCheck} />
          ) : (
            <FontAwesomeIcon icon={faEdit} />
          )}
        </button>
      </div>
      <div>
        <h5>Tipos</h5>
        <Labels
          items={pokemon?.types ?? []}
          onDeleteItem={type => {
            dispatch(
              updatePokemon({
                ...pokemon,
                types: (pokemon?.types ?? []).filter(tp => tp !== type),
              })
            );
          }}
          onNewItem={type => {
            const newTypes = (pokemon?.types ?? []).concat([type]);
            dispatch(
              updatePokemon({
                ...pokemon,
                types: newTypes,
              })
            );
          }}
        />
      </div>
      <div id="attacks-fast">
        <h5>Ataques rápidos</h5>
        <Labels
          items={(pokemon?.attacks?.fast ?? []).map(atk => atk.name)}
          onNewItem={atk => {
            const newFastAtks = (pokemon?.attacks?.fast ?? []).concat([
              { name: atk },
            ]);
            dispatch(
              updatePokemon({
                ...pokemon,
                attacks: {
                  ...pokemon?.attacks,
                  fast: newFastAtks,
                },
              })
            );
          }}
          onDeleteItem={atk => {
            dispatch(
              updatePokemon({
                ...pokemon,
                attacks: {
                  ...pokemon?.attacks,
                  fast: (pokemon?.attacks.fast ?? []).filter(
                    attack => attack.name !== atk
                  ),
                },
              })
            );
          }}
        />
      </div>
      <div id="attacks-special">
        <h5>Ataques especias</h5>
        <Labels
          items={(pokemon?.attacks?.special ?? []).map(atk => atk.name)}
          onNewItem={atk => {
            const newSpecialAtks = (pokemon?.attacks?.special ?? []).concat([
              { name: atk },
            ]);
            dispatch(
              updatePokemon({
                ...pokemon,
                attacks: {
                  ...pokemon?.attacks,
                  special: newSpecialAtks,
                },
              })
            );
          }}
          onDeleteItem={atk => {
            dispatch(
              updatePokemon({
                ...pokemon,
                attacks: {
                  ...pokemon?.attacks,
                  special: (pokemon?.attacks.special ?? []).filter(
                    attack => attack.name !== atk
                  ),
                },
              })
            );
          }}
        />
      </div>
    </StyledDiv>
  );
};

export default PokePage;

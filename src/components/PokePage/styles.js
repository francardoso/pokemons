import styled from 'styled-components/macro';

export default styled.div`
  margin: 10px 200px;
  border: 3px double black;
  padding: 10px;
  #img-container {
    display: flex;
    justify-content: center;
    height: 200px;
    img {
      object-fit: contain;
      max-width: 200px;
      max-height: 200px;
    }
  }
  h5 {
    margin: 5px 0;
  }
  #name {
    margin: 5px;
    p {
      font-weight: 800;
      text-decoration: underline;
    }
  }
`;

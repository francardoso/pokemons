import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

// Pages
import Pokemons from './views/Pokemons';
import Pokemon from './views/Pokemon';
import Page404 from './views/Page404';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          <Pokemons />
        </Route>
        <Route path="/:idPokemon" exact>
          <Pokemon />
        </Route>
        <Route path="*">
          <Page404 />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;

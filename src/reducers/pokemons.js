import { SET_POKEMONS, UPDATE_POKEMON } from '../actions/pokemons';

const initialState = {
  pokemons: {},
};

const pokemonsReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_POKEMONS: {
      return {
        ...state,
        pokemons: action.payload,
      };
    }
    case UPDATE_POKEMON: {
      return {
        ...state,
        pokemons: {
          ...state.pokemons,
          [action?.payload?.id]: action.payload,
        },
      };
    }
    default:
      return state;
  }
};

export default pokemonsReducer;
